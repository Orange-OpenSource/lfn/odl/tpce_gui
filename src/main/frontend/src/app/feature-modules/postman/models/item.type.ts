import { Folder } from './folder.model';
import { Request } from './request.model';

export type Item = (Request | Folder);
