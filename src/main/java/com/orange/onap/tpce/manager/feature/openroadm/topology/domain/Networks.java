package com.orange.onap.tpce.manager.feature.openroadm.topology.domain;

import lombok.Getter;

@Getter
public class Networks {
    private RawNetwork networks;
}
