package com.orange.onap.tpce.manager;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class OnapTpceManagerApplication {

	public static void main(String[] args) {
		SpringApplication.run(OnapTpceManagerApplication.class, args);
	}
}





