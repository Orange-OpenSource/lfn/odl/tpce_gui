package com.orange.onap.tpce.manager.feature.postman.model;

public enum HttpMethod {
    GET,
    POST,
    PUT,
    PATCH,
    DELETE,
    HEAD
}
