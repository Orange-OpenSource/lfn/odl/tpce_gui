package com.orange.onap.tpce.manager.feature.services.domain.service.path;

import lombok.Getter;

@Getter
public class Node {
    private String id;
    private Resource resource;
}
